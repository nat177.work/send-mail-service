const Bull = require("bull");
const NodeMailerService = require("./NodeMailerService");
const MailServices = require("./MailServices");
const opts = {
  redis: {
    port: process.env.REDIS_PORT || 6379,
    host: process.env.REDIS_HOST || "localhost",
  },
};

const sendMailQueue = new Bull("sendMail");

sendMailQueue.process(async (job, done) => {
  try {
    const { mailOptions } = job.data;
    console.log(JSON.stringify(mailOptions));
    // await NodeMailerService.sendMail(mailOptions);
    await MailServices.sendEmail(mailOptions);
    done();
  } catch (error) {
    sails.log.error(error);
    return done(error);
  }
});
