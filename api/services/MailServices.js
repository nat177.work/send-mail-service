const axios = require("axios");
module.exports = {
  sendEmail: async (mailOptions) => {
    try {
      await axios.post("http://127.0.0.1:1880/send-email", mailOptions);
    } catch (error) {
      sails.log.error(error);
    }
  },
};
