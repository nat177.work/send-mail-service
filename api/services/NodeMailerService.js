const nodeMailer = require("nodemailer");
const transporter = nodeMailer.createTransport({
  host: process.env.SMTP_HOST,
  port: process.env.SMTP_PORT,
  auth: {
    user: process.env.SMTP_USER,
    pass: process.env.SMTP_PASS,
  },
});
module.exports = {
  sendMail: (mailOptions) => {
    return transporter.sendMail(mailOptions);
  },
};
